# containers/docker

**[Docker Hub Repository](https://hub.docker.com/r/naiithink/public "Visit Docker Hub Repository")**

## `Dockerfile` Links

| Tag | Created | Platform |
| :-- | :------ | :------- |
| [`latest`, `ubuntu-jammy`](containers/debian/ubuntu-22.04.Dockerfile) | 2022-05-26 | `linux/arm64`, `linux/amd64` |
| `ubuntu-focal` | 2022-03-13 | `linux/arm64`, `linux/amd64` |
